# acelee-sentinel-dashboard-apollo

#### 介绍
Alibaba-Sentinel dashboard源码，简单修改了限流规则的推拉，能实现与Apollo的修改同步，开箱即用！

#### 软件架构

spring boot 2.x 

spring cloud alibaba sentinel-dashboard 1.6.2 

#### 使用说明

1. 启动 DashboardApplication类的main方法即可

#### 参与贡献

   欢迎关注star，谢谢！
   
   **使用参考**作者博文 [Spring Cloud Alibaba（6）Sentinel Dashboard中修改规则同步到Apollo 带源码](https://blog.csdn.net/las723/article/details/95164570)